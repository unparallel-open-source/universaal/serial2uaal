package utils;

import org.universAAL.middleware.context.ContextEvent;
import org.universAAL.ontology.che.ContextHistoryService;

public class Constants {
	/* ------------------------------ ACTIVATOR CONSTANTS ------------------------------ */
	public  static final int      START_TIME = 5/*30*/; // in seconds
	public  static final int      REPEAT_TIME = 5/*60*/; // in seconds
	
	/* ----------------------------- REST CLIENT CONSTANTS ----------------------------- */
	public  static final String   DATA_URL = "http://localhost:%d/values"/*"http://10.0.1.80:%d/values"*//*"http://10.0.1.35:%d/values"*/;
	public  static final String   GET_REQUEST = "GET";
	public  static final int      OK = 200;
	
	/* ---------------------------------- DB CONSTANTS --------------------------------- */
	private static final String   UNIVERSAAL_ONTOLOGY_URI = "http://ontology.universAAL.org/";
	private static final String   UNIVERSAAL_REST_NAMESPACE = "urn:org.universAAL.rest:";
	public  static final String   ABSOLUTE_TIMESTAMP_X73_ONTOLOGY = UNIVERSAAL_ONTOLOGY_URI + "X73.owl#absoluteTimeStamp";
	public  static final String   ONTOLOGY_PREFIX = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>";
	public  static final String   CONTEXT_EVENT_SUBJECT_TYPE_URI = UNIVERSAAL_ONTOLOGY_URI + "Context.owl#ContextEvent";
	public  static final String   HAS_VALUE_PREDICATE_URI = UNIVERSAAL_ONTOLOGY_URI + "Device.owl#hasValue";
	public  static final String   PROPERTY_PROCESSES = ContextHistoryService.PROP_PROCESSES;
	public  static final String[] PROPERTY_MANAGES = new String[] { ContextHistoryService.PROP_MANAGES };
	public  static final String   TIMESTAMP_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	public  static final String   LAST_SYNC_URI = UNIVERSAAL_REST_NAMESPACE + "LastSync";
	
	/* ------------------------------ MIDDLEWARE CONSTANTS ----------------------------- */
	public  static final String   HEART_RATE_URI = UNIVERSAAL_REST_NAMESPACE + "HeartRate";
	public  static final String   CONTEXT_URI = "urn:org.universAAL.rest:rest.connector";
	public  static final String   CONTEXT_EVENT_URI = ContextEvent.CONTEXT_EVENT_URI_PREFIX + "ContextEvent";
}