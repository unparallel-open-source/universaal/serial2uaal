package middleware;

import java.text.ParseException;

import org.universAAL.context.che.Hub;
import org.universAAL.context.che.Hub.Log;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.context.ContextEvent;
import org.universAAL.middleware.context.ContextPublisher;
import org.universAAL.middleware.context.owl.ContextProvider;
import org.universAAL.middleware.context.owl.ContextProviderType;
import org.universAAL.middleware.rdf.Resource;
import org.universAAL.ontology.device.ValueDevice;

import ontologies.SPLFastSensor;
import ontologies.SPLSlowSensor;
import serial.Serial;
import utils.Constants;

public class Middleware {
	/* ---------------------------------- VARIABLES ----------------------------------- */
	private static Log log;
	private ModuleContext moduleContext;
	
	/* --------------------------------- CONSTRUCTOR ---------------------------------- */
	public Middleware(ModuleContext moduleContext) {
		log = Hub.getLog(Middleware.class);
		this.moduleContext = moduleContext;
	}
	
	public void publishData(String data, int dataType) throws IllegalArgumentException, ParseException {

		switch(dataType) {
			case Serial.SPL_SLOW: {
				log.info("publishData", "Publishing SPL Slow Event: " + data);
				publishEvent(data, new SPLSlowSensor("urn:org.universAAL.rest:SPLSlow"));
				
				break;
			}
			case Serial.SPL_FAST: {
				log.info("publishData", "Publishing SPL Fast Event: " + data);
				publishEvent(data, new SPLFastSensor("urn:org.universAAL.rest:SPLFast"));
				
				break;
			}
			default: break;
		}
	}
	
	private void publishEvent(String data, Resource subject) {
		ContextProvider contextProvider = getContextProvider();
		ContextEvent event = generateContextEvent(data, subject, contextProvider);
		ContextPublisher publisher = getContextPublisher(contextProvider);
		
		publisher.publish(event);
		publisher.close();
	}
	
	private ContextProvider getContextProvider() {
		ContextProvider contextProvider = new ContextProvider(Constants.CONTEXT_URI);
		contextProvider.setType(ContextProviderType.gauge);
		
		return contextProvider;
	}
	
	private ContextPublisher getContextPublisher(ContextProvider contextProvider) throws IllegalArgumentException {
		ContextPublisher publisher = new ContextPublisher(moduleContext, contextProvider) {
			@Override public void communicationChannelBroken() {
				close();
				log.warn("getContextPublisher", "Publisher's Communication Channel is now broken!");
			}
		};
		
		return publisher;
	}
	
	private ContextEvent generateContextEvent(String data, Resource subject, ContextProvider contextProvider) {
		ContextEvent event = new ContextEvent(Constants.CONTEXT_EVENT_URI);
		event.setRDFSubject(subject);
		event.setRDFPredicate(ValueDevice.PROP_HAS_VALUE);
		event.setRDFObject(data);
		event.setResourceLabel("Label");
		
		return event;
	}
}
