package serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.TooManyListenersException;

import org.universAAL.middleware.container.ModuleContext;

import gnu.io.*;
import middleware.Middleware;

public class Serial implements SerialPortEventListener {

	//for containing the ports that will be found
    private Enumeration<?> ports;
    //map the port names to CommPortIdentifiers
    private Map<String, CommPortIdentifier> portMap;

    //this is the object that contains the opened port
    private CommPortIdentifier selectedPortIdentifier;
    private SerialPort serialPort;

    //input and output streams for sending and receiving data
    private InputStream input;
    private OutputStream output;

    private Middleware middleware;

    //the timeout value for connecting with the port
    private final static int TIMEOUT = 2000;
    
    // data types
    public final static int SPL_SLOW = 0;
    public final static int SPL_FAST = 1;

	public Serial(ModuleContext moduleContext) {
		ports = null;
		portMap = new HashMap<String, CommPortIdentifier>();
		selectedPortIdentifier = null;
		serialPort = null;
		input = null;
		output = null;
		middleware = new Middleware(moduleContext);
	}
    
	@Override public void serialEvent(SerialPortEvent event) {
		if(event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			System.out.println("Ready to read new data!");
			try {
				int data;
				System.out.print("New Data: ");
				while((data = input.read()) > -1) {
					System.out.print(new String(new byte[] { (byte)data }));
				}
				System.out.print("Data has been read!");
			} catch (IOException e) {
				System.out.println("Error on reading data!");
				e.printStackTrace();
			}
		}
	}
	
	public void read(int dataType) {
		try {
			int data;
			
			String value = "";
			System.out.print("New Data: ");
			while((data = input.read()) > -1) {
				value = value.concat(new String(new byte[] { (byte)data }));
				System.out.print(value);
				
				if(data == 10) { // detect "\n" ASCII code
					break;
				}
			}
			
			middleware.publishData(value, dataType);
			//System.out.println("Data has been read!");
		} catch (IOException | IllegalArgumentException | ParseException e) {
			System.out.println("Error on reading data!");
			e.printStackTrace();
		}
	}
	
	public Map<String, CommPortIdentifier> getPorts() {
		return portMap;
	}
	
	public boolean hasPort(String portToFind) {
		for(String port : getPorts().keySet()) {
			if(portToFind.equals(port)) {
				return true;
			}
		}
		
		return false;
	}
	
	public void writeData(String command) {
		byte[] data = command.getBytes();
		try {
			output.write(data);
			output.flush();
			//System.out.println("\n\n\nData written\n\n\n");
		} catch (IOException e) {
			System.out.println("Error on writing data!");
			e.printStackTrace();
		}
	}
	
	public void searchForSerialPorts() {
		System.out.println("\n\n\nSearching ports...\n\n\n");
		ports = CommPortIdentifier.getPortIdentifiers();
		
		while(ports.hasMoreElements()) {
			CommPortIdentifier currentPort = (CommPortIdentifier)ports.nextElement();
			
			// To only get Serial ports
			if(currentPort.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				portMap.put(currentPort.getName(), currentPort);
				System.out.println("\n\n\nAdded port " + currentPort.getName() + "\n\n\n");
			}
		}
	}
	
	public void connect(String selectedPort) {
		selectedPortIdentifier = portMap.get(selectedPort);
		
		try {
			CommPort commPort = selectedPortIdentifier.open(this.getClass().getName(), TIMEOUT);
			serialPort = (SerialPort)commPort;
			System.out.println(selectedPort + " opened successfully!!");
		} catch (PortInUseException e) {
			System.out.println(selectedPort + " is already in use!");
			e.printStackTrace();
		}
	}
	
	public boolean initStreams() {
		try {
			synchronized(serialPort) {
				input = serialPort.getInputStream();
				output = serialPort.getOutputStream();
				System.out.println("\n\n\nStreams initialized!\n\n\n");
				return true;
			}
		} catch(IOException e) {
			System.out.println("IOException at initStreams()");
			e.printStackTrace();
			return false;
		}
	}
	
	public void initListener() {
		try {
			synchronized(serialPort) {
				serialPort.addEventListener(this);
				serialPort.notifyOnDataAvailable(true);
				System.out.println("\n\n\nListener initialized!\n\n\n");
			}
		} catch(TooManyListenersException e) {
			System.out.println("Too many listeners!");
			e.printStackTrace();
		}
	}
	
	public void disconnect() {
		try {
			synchronized(serialPort) {
				input.close();
				output.close();
				serialPort.removeEventListener();
				serialPort.close();
				System.out.println("Disconnecting...");
			}
		} catch(IOException e) {
			System.out.println("Failed to close port " + serialPort.getName());
			e.printStackTrace();
		}	
	}
}