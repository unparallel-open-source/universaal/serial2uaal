package mainpackage;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.container.osgi.OSGiContainer;
import org.universAAL.middleware.owl.OntologyManagement;

import ontologies.SPLSensorOntology;
import serial.Serial;

public class Activator implements BundleActivator {
	
	private static final String SPL_PORT = "/dev/ttyS2";
	
	public static ModuleContext moduleContext;
	private static Serial serial;
	private static ScheduledExecutorService scheduler;
	private SPLSensorOntology splOntology;
	
	@Override
	public void start(BundleContext bundleContext) throws Exception {
		moduleContext = OSGiContainer.THE_CONTAINER.registerModule(new Object[] { bundleContext });
		splOntology = new SPLSensorOntology();
		
		setupOntologies();
		
		serial = new Serial(moduleContext);
		
		System.out.println("\n\n\nStarting...\n\n\n");
		
		synchronized (serial) {
			serial.searchForSerialPorts();
			
			if(serial.hasPort(SPL_PORT)) {
				serial.connect(SPL_PORT);
				
				if(serial.initStreams()) {
					requestDataPeriodically();
				}
			}	
		}
	}
	
	private static void requestDataPeriodically() {
		scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(new Runnable() {
			@Override public void run() {
				synchronized (serial) {
					serial.writeData("SPL:GET S\n");
					serial.read(Serial.SPL_SLOW);
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						System.err.println("Interrupted thread");
					}
					serial.writeData("SPL:GET F\n");
					serial.read(Serial.SPL_FAST);
					
				}
			}
		}, 0, 3, TimeUnit.SECONDS);
	}
	
	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		scheduler.shutdown();
		
		try {
			if(!scheduler.awaitTermination(2, TimeUnit.SECONDS)) {
				scheduler.shutdownNow();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		synchronized (serial) {
			serial.disconnect();
		}
		
		unregisterOntologies();
		
        System.out.println("Disconnected!");
	}
	
	private void setupOntologies() {
		unregisterOntologies();  // Just in case the module has not been stopped properly
		OntologyManagement.getInstance().register(moduleContext, splOntology);
	}
	
	private void unregisterOntologies() {
		OntologyManagement.getInstance().unregister(moduleContext, splOntology);
	}

}
