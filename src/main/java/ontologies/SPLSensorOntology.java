package ontologies;

import org.universAAL.middleware.owl.DataRepOntology;
import org.universAAL.middleware.owl.ManagedIndividual;
import org.universAAL.middleware.owl.MergedRestriction;
import org.universAAL.middleware.owl.OntClassInfoSetup;
import org.universAAL.middleware.owl.Ontology;
import org.universAAL.middleware.rdf.Resource;
import org.universAAL.middleware.service.owl.ServiceBusOntology;
import org.universAAL.ontology.device.DeviceOntology;
import org.universAAL.ontology.location.LocationOntology;
import org.universAAL.ontology.measurement.MeasurementOntology;
import org.universAAL.ontology.phThing.PhThingOntology;

public class SPLSensorOntology extends Ontology {

	public static final String NAMESPACE = "http://ontology.universAAL.org/Device.owl#";
	
	private static SensorFactory sensorFactory = new SensorFactory();
	
	public SPLSensorOntology() {
		super(NAMESPACE);
	}
	
	public SPLSensorOntology(String ontologyUri) {
		super(ontologyUri);
	}

	@Override public void create() {
		// Information for the whole ontology
		Resource resource = getInfo();
		resource.setResourceComment("The Collection of SPL Sensors");
		resource.setResourceLabel("SPL Sensors");
		
		// Add imports for every ontology domain used for SPL concepts
		addImport(DataRepOntology.NAMESPACE);
		addImport(ServiceBusOntology.NAMESPACE);
		addImport(LocationOntology.NAMESPACE);
		addImport(MeasurementOntology.NAMESPACE);
		addImport(PhThingOntology.NAMESPACE);
		addImport(DeviceOntology.NAMESPACE);
		
		// Load SPLFastSensor class and basic information
		OntClassInfoSetup ontologyInfo = createNewOntClassInfo(SPLFastSensor.MY_URI, sensorFactory, 0);
		ontologyInfo.setResourceComment("SPL Fast Sensor Event");
		ontologyInfo.setResourceLabel("SPL Fast Sensor");
		ontologyInfo.addSuperClass(ManagedIndividual.MY_URI);
		
		// Restrictions for SPLFastSensor
		ontologyInfo.addObjectProperty(SPLFastSensor.PROP_HAS_VALUE);
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(SPLFastSensor.PROP_HAS_VALUE, SPLFastSensor.MY_URI));
		
		// Load SPLSlowSensor class and basic information
		ontologyInfo = createNewOntClassInfo(SPLSlowSensor.MY_URI, sensorFactory, 1);
		ontologyInfo.setResourceComment("SPL Slow Sensor Event");
		ontologyInfo.setResourceLabel("SPL Slow Sensor");
		ontologyInfo.addSuperClass(ManagedIndividual.MY_URI);
				
		// Restrictions for SPLSlowSensor
		ontologyInfo.addObjectProperty(SPLSlowSensor.PROP_HAS_VALUE);
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(SPLSlowSensor.PROP_HAS_VALUE, SPLSlowSensor.MY_URI));
		
		/*ontologyInfo.addDatatypeProperty(SPLFastSensor.PROP_SPL);
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(SPLFastSensor.PROP_SPL, new IntRestriction(0, true, 100, true)));*/
	}
}
