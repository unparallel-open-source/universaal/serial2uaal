package ontologies;

import org.universAAL.middleware.owl.IntRestriction;
import org.universAAL.middleware.owl.MergedRestriction;
import org.universAAL.middleware.owl.OntClassInfoSetup;
import org.universAAL.middleware.owl.Ontology;
import org.universAAL.middleware.rdf.Resource;
import org.universAAL.ontology.device.DeviceOntology;
import org.universAAL.ontology.device.Sensor;

public class SPLSlowSensorOntology extends Ontology {

	public static final String NAMESPACE = "http://ontology.universAAL.org/Device.owl#";
	
	private static SensorFactory sensorFactory = new SensorFactory();
	
	public SPLSlowSensorOntology() {
		super(NAMESPACE);
	}

	@Override public void create() {
		Resource resource = getInfo();
		resource.setResourceComment("Sensor Event");
		resource.setResourceLabel("Sensor");
		addImport(DeviceOntology.NAMESPACE);
		
		OntClassInfoSetup ontologyInfo = createNewOntClassInfo(SPLSlowSensor.MY_URI, sensorFactory, 1);
		ontologyInfo.setResourceComment("SPL Slow Sensor Event");
		ontologyInfo.setResourceLabel("SPL Slow Sensor");
		ontologyInfo.addSuperClass(Sensor.MY_URI);
		ontologyInfo.addObjectProperty(SPLSlowSensor.PROP_HAS_VALUE).setFunctional();
		ontologyInfo.addDatatypeProperty(SPLSlowSensor.PROP_SPL).setFunctional();
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(SPLSlowSensor.PROP_HAS_VALUE, SPLSlowSensor.MY_URI));
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(SPLSlowSensor.PROP_SPL, new IntRestriction(0, true, 100, true)));
	}
}
