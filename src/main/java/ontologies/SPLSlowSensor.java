package ontologies;

import org.universAAL.middleware.owl.ManagedIndividual;

public class SPLSlowSensor extends ManagedIndividual {

	public static final String MY_URI = SPLSensorOntology.NAMESPACE + "SPLSlowSensor";
	public static final String PROP_HAS_VALUE = SPLSensorOntology.NAMESPACE + "hasValue";
	public static final String PROP_HAS_TYPE = SPLSensorOntology.NAMESPACE + "hasType";
	public static final String PROP_SPL = SPLSensorOntology.NAMESPACE + "splslow";
	
	public SPLSlowSensor() {
		super();
	}
	
	public SPLSlowSensor(String instanceURI) {
		super(instanceURI);
	}
	
	public String getClassURI() {
		return MY_URI;
	}

	public int getPropSerializationType(String propURI) {
		return PROP_SERIALIZATION_FULL;
	}

	public boolean isWellFormed() {
		return true;
	}
}
